﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio2
{
    class Program
    {
        static void Main(string[] args)

          /*Crea un programa que escriba en pantalla los números del 1 al 10, usando "do..while".*/
        {
            int x = 1;
            do
            {
                Console.WriteLine($"{x}");
                x++;
            } while (x <= 10);

            Console.ReadKey();
        }
    }
}
