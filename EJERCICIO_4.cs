﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio4
{
    class Program
    {
        static void Main(string[] args)

            //Crear un programa que muestre las letras de la Z(mayúscula) a la A(mayúscula, descendiendo).

        {

            char letra;

            for (letra = 'Z'; letra >= 'A'; letra--)
                Console.WriteLine("{0} ", letra);

            Console.ReadKey();
        }
    }
}
