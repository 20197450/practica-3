﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio1
{
    class Program
    {
        static void Main(string[] args)

        {
            /*Crear un programa que pida números positivos al usuario, y vaya calculando
            la suma de todos ellos (terminará cuando se teclea un número negativo o cero).*/

            int N, S = 0;

            Console.WriteLine("Este programa permite ingresar infinitos numeros positivos," +
                    " si desea realizar la sumar y terminar el programa introduzca un numero negativo");
            Console.WriteLine("********************************************************************************************************************");
            do

            {
               

                Console.Write("Ingrese numero:");

                N = int.Parse(Console.ReadLine());

                S = S + N;

            }

            while (N > 0);

            {

                Console.WriteLine("La suma de todos los numeros es:  {0}", S);

                Console.ReadKey();

            }

        }

    }

}
